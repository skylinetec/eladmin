package me.zhengjie.annotation;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=Test.cc.class)
public @interface Test {
    String color() default "res";
    class cc implements ConstraintValidator<Test, Object> {
        String message= "参数校验不通过，请重新输入";
        Class<?>[] groups={};
        @Override
        public void initialize(Test constraintAnnotation) {
            System.out.println("my validator init");
        }

        @Override
        public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
            if(o == null){
                return true;
            }
            return false;
        }

    }
}
